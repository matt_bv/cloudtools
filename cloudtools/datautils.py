# -*- coding: utf-8 -*-
"""
Basic module to process point clouds.

@author: Matheus Boni Vicari (matheus.boni.vicari@gmail.com or
                              matheus.vicari.15@ucl.ac.uk)
"""

import numpy as np
from sklearn.neighbors import NearestNeighbors


def upscale_data(low_dens_points, high_dens_points, dist_threshold,
                 return_ids=False):

    """
    Function to upscale parameters from a less dense point cloud to a higher
    density point cloud.

    Parameters
    ----------
    low_dens_points: array
        (Relatively) Low density point cloud with shape n x m, where n is the
        number of points and m is the coordinates and parameters. Therefore
        m >= 3. The data should have the following columns layout: (x, y, z,
        ...).

    high_dens_points: array
        (Relatively) High density point cloud with shape n x m, where n is the
        number of points and m is the coordinates and parameters. Therefore
        m >= 3. The data should have the following columns layout: (x, y, z,
        ...).

    dist_threshold: scalar
        Maximum distance of high_dens_points from points in low_dens_points to
        still be valid for upscaling. Aims to avoid applying parameters from
        low_dens_points to points that are too far away.

    return_ids: bool
        If True, the function will return ids of low_dens_points mapped into
        high_dens_points. If False, return upscaled point cloud. Default is
        False.

    Returns
    -------
    idx[mask]: array
        Indices of low_dens_points mapped into high_dens_points.

    or

    high_dens_points[mask]: array
        Upscaled points from low_dens_points.

    """

    # Setting up nearest neighbors search using only one neighbor to find
    # the closest point from low_dens_points to high_dens_points.
    nbrs = NearestNeighbors(leaf_size=30, n_jobs=-1).fit(low_dens_points)
    dist, idx = nbrs.kneighbors(high_dens_points, n_neighbors=1)

    # Obtaining indices of high_den_points that are closer to low_dens_points
    # than the distance threshold.
    mask = np.where(dist <= dist_threshold)[0]

    # Returning either the masked indices or the masked points + parameters.
    if return_ids is True:
        return idx[mask]
    elif return_ids is False:
        return high_dens_points[mask]


def correct_RGB_knn(correct_data, bad_data, knn=3, avg_type='simple'):

    """
    Function to correct points with bad RGB data (saturated) by averaging
    a fixed number of neighboring RGB values.

    Parameters
    ----------
    correct_data: array
        Indices of points with good RGB data to be used as filler.
    bad_data: array
        Indices of points with bad RGB data to be corrected.
    knn: scalar
        Number of neighboring points around each point in bad_data to select
        for correction of RGB values. Default is 3.
    avg_type: string
        Option to select the averaging method to be used. Default is 'simple'.

    Returns
    -------
    corrected_data: array
        Corrected point cloud data of shape n x 6, where n is the same number
        of points in bad_data. The data will have the following columns layout:
        (x, y, z, R, G, B).

    """

    # Setting up nearest neighbors search to find the knn closest points from
    # correct_data to bad_data. The neighbors search uses only x, y and z
    # information.
    nbrs = NearestNeighbors(leaf_size=30, n_jobs=-1).fit(correct_data[:, :3])
    dist, idx = nbrs.kneighbors(bad_data[:, :3], n_neighbors=knn)

    # Initializing temporary corrected_rgb variable.
    corrected_rgb = np.zeros([bad_data.shape[0], 3])

    # Checking averaging methodology option and correcting RGB values from
    # bad_data.
    if avg_type == 'simple':
        # Performing simple averaging of R, G and B neighboring values around
        # each point in bad_data and assigning them to corrected_rgb.
        for row in xrange(idx.shape[0]):
            corrected_rgb[row, :] = np.mean(correct_data[idx[row], 3:6],
                                            axis=0)

    elif avg_type == 'weighted':
        # Performing weighted averaging of R, G and B neighboring values around
        # each point in bad_data and assigning them to corrected_rgb. The
        # weight is defined by the inverse distance of neighboring
        # points from center (bad_data) points.
        for d, i, row in zip(dist, idx, xrange(idx.shape[0])):
            # Calculating weight.
            w = 1 / d
            # Averaging RGB values.
            corrected_rgb[row, :] = np.average(correct_data[i, 3:6], weights=w,
                                               axis=0)

    # Initializing output variables and assigning corrected RGB values to
    # columns 3 (R), 4 (G) and 5  (B).
    corrected_data = bad_data.copy()
    corrected_data[:, 3:6] = corrected_rgb

    return corrected_data


def correct_RGB_rad(correct_data, bad_data, rad=0.05, avg_type='simple'):

    """
    Function to correct points with bad RGB data (saturated) by averaging
    a variable number, defined by a fixed radius, of neighboring RGB values.

    Parameters
    ----------
    correct_data: array
        Indices of points with good RGB data to be used as filler.
    bad_data: array
        Indices of points with bad RGB data to be corrected.
    rad: scalar
        Radius to seach for neighboring points around each point in
        bad_data to select for correction of RGB values. Default is 0.05.
    avg_type: string
        Option to select the averaging method to be used. Default is 'simple'.

    Returns
    -------
    corrected_data: array
        Corrected point cloud data of shape n x 6, where n is the same number
        of points in bad_data. The data will have the following columns layout:
        (x, y, z, R, G, B).

    """

    # Setting up nearest neighbors search to find the closest points from
    # correct_data to bad_data. The neighbors search uses only x, y and z
    # information.
    nbrs = NearestNeighbors(leaf_size=30, n_jobs=-1).fit(correct_data[:, :3])
    dist, idx = nbrs.radius_neighbors(bad_data[:, :3], radius=rad)

    # Initializing temporary corrected_rgb variable.
    corrected_rgb = np.zeros([bad_data.shape[0], 3])

    # Checking averaging methodology option and correcting RGB values from
    # bad_data.
    if avg_type == 'simple':
        # Performing simple averaging of R, G and B neighboring values around
        # each point in bad_data and assigning them to corrected_rgb.
        for row in xrange(idx.shape[0]):
            corrected_rgb[row, :] = np.mean(correct_data[idx[row], 3:6],
                                            axis=0)

    elif avg_type == 'weighted':
        # Performing weighted averaging of R, G and B neighboring values around
        # each point in bad_data and assigning them to corrected_rgb. The
        # weight is defined by the inverse distance of neighboring
        # points from center (bad_data) points.
        for d, i, row in zip(dist, idx, xrange(idx.shape[0])):

            # Checking the number of neighboring points.
            if i.shape[0] >= 2:
                # If equal or larger than 2, perform weighted average.
                # Calculating weight.
                w = 1 / d
                corrected_rgb[row, :] = np.average(correct_data[i, 3:6],
                                                   weights=w, axis=0)
            elif i.shape[0] == 1:
                # If current bad_data point has only one neighbor found, fill
                # corrected_rbg with its data.
                corrected_rgb[row, :] = correct_data[i, 3:6]
            else:
                # If no neighbors, use incorrect RGB data anyway.
                corrected_rgb[row, :] = bad_data[row, 3:6]

    # Initializing output variables and assigning corrected RGB values to
    # columns 3 (R), 4 (G) and 5  (B).
    corrected_data = bad_data.copy()
    corrected_data[:, 3:6] = corrected_rgb

    return corrected_data


def mask_bad_RGB(point_data, rgb_threshold=150):

    """
    Function to mask points with good or bad RGB values.

    Parameters
    ----------
    point_data: array
        Point cloud data of shape n x 6, where n is the number of points. The
        data should have the following columns layout: (x, y, z, R, G, B).
    rgb_threshold: scalar
        Mean RGB value to use as minimum threshold to identify points with
        bad RGB values (from saturated pixels).

    Returns
    -------
    good_points_ids: array
        Indices of points with good RGB data within point_data.
    bad_points_ids: array
        Indices of points with bad RGB data within point_data.

    """

    # Calculating a mean rgb_value for each point.
    mean_vals = np.mean(point_data[:, 3:], axis=1)

    # Detecting bad points.
    mask = mean_vals <= rgb_threshold

    # Assigning valid good and bad points.
    good_points_ids = np.where(mask)[0]
    bad_points_ids = np.where(~mask)[0]

    return good_points_ids, bad_points_ids
